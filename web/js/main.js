//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
//
//      MAIN DOCUMENT READY SCRIPT OF CONTROL CENTER
//
//      Copyright Recursive Software Development OU
//      http://www.recursive.ee
//
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////

var _wait;

if(!window.jQuery)
        alert('Ouch! Impossibile caricare jQuery.');


$(document).ready(function () {
    console.debug('Questa web application e sviluppata da Recursive software -> vienici a trovare su http://www.recursive.ee');
    console.debug('This web application is developed by Recursive software -> visit us at http://www.recursive.ee');

    setupAjaxMessage()




	$('.show-sidebar').on('click', function () {
		$('div#main').toggleClass('sidebar-show');
		setTimeout(MessagesMenuWidth, 250);
	});

    $('a').click(function (e){
        if ($(this).attr('href') == '#') {
			e.preventDefault();
		}
     });

	$('.main-menu').on('click', 'a', function (e) {
		var parents = $(this).parents('li');
		var li = $(this).closest('li.dropdown');
		var another_items = $('.main-menu li').not(parents);
		another_items.find('a').removeClass('active');
		another_items.find('a').removeClass('active-parent');
		if ($(this).hasClass('dropdown-toggle') || $(this).closest('li').find('ul').length == 0) {
			$(this).addClass('active-parent');
			var current = $(this).next();
			if (current.is(':visible')) {
				li.find("ul.dropdown-menu").slideUp('fast');
				li.find("ul.dropdown-menu a").removeClass('active')
			}
			else {
				another_items.find("ul.dropdown-menu").slideUp('fast');
				current.slideDown('fast');
			}
		}
		else {
			if (li.find('a.dropdown-toggle').hasClass('active-parent')) {
				var pre = $(this).closest('ul.dropdown-menu');
				pre.find("li.dropdown").not($(this).closest('li')).find('ul.dropdown-menu').slideUp('fast');
			}
		}
		if ($(this).hasClass('active') == false) {
			$(this).parents("ul.dropdown-menu").find('a').removeClass('active');
			$(this).addClass('active')
		}


		if ($(this).attr('href') == '#') {
			e.preventDefault();
		}
	});
	var height = window.innerHeight - 49;
	$('#main').css('min-height', height)
		.on('click', '.expand-link', function (e) {
			var body = $('body');
			e.preventDefault();
			var box = $(this).closest('div.box');
			var button = $(this).find('i');
			button.toggleClass('fa-expand').toggleClass('fa-compress');
			box.toggleClass('expanded');
			body.toggleClass('body-expanded');
			var timeout = 0;
			if (body.hasClass('body-expanded')) {
				timeout = 100;
			}
			setTimeout(function () {
				box.toggleClass('expanded-padding');
			}, timeout);
			setTimeout(function () {
				box.resize();
				box.find('[id^=map-]').resize();
			}, timeout + 50);
		})
		.on('click', '.collapse-link', function (e) {
			e.preventDefault();
			var box = $(this).closest('div.box');
			var button = $(this).find('i');
			var content = box.find('div.box-content');
			content.slideToggle('fast');
			button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
			setTimeout(function () {
				box.resize();
				box.find('[id^=map-]').resize();
			}, 50);
		})
		.on('click', '.close-link', function (e) {
			e.preventDefault();
			var content = $(this).closest('div.box');
			content.remove();
		});

	$('body').on('click', 'a.close-link', function(e){
		e.preventDefault();
		CloseModalBox();
	});


});

function setupAjaxMessage() {
      _cover='<div id="coverLayer" style="position:absolute; width:100%; height:100%; left:0px; top:0px; background-color:rgba(0,0,0,0.3); display:none; z-index:10000;"></div>';
      _dvs='<div id="facebookG"><div id="blockG_1" class="facebook_blockG"></div><div id="blockG_2" class="facebook_blockG"></div><div id="blockG_3" class="facebook_blockG"></div></div>';
      $('#content').append(_cover);

      // Global first Ajax request initiate signal > parte solo al primo giro
      $( document ).ajaxStart(function() {
      var opts = {
        title: _dvs+"&nbsp;&nbsp;Caricamento",
        text: "",
        icon: false,
        type : "notice",
         buttons: {
            closer: false,
            sticker: false
            },
        hide: false
        };
        _wait = new PNotify(opts);
        $('#coverLayer').fadeIn(300);
    });

    // Global Ajax request termination signal
    $( document ).ajaxStop(function() {
      if(_wait) {
        _wait.remove();
        $('#coverLayer').fadeOut(300);
      }
    });

    // Ajax Global Error management - specificare casi particolari nelle singole App
    $( document ).ajaxError(function( event, jqxhr, settings, exception ) {
            showError(jqxhr.statusText);
    });



    setInterval(function(){

      if(_wait)
        _wait.update({text : "&nbsp;&nbsp;Esecuzione di "+$.active+" script"+($.active > 1 ? 's':'')+"..."});
      },500);


}

function showMessage(message) {
   new PNotify({
		text: message,
        icon: 'fa fa-info-circle',
        type: 'info',
        delay: 1500
	});
}

function showError(error) {
   new PNotify({
		text: error,
        icon: 'fa fa-exclamation-triangle',
        type: 'error',
        hide: false,
        buttons: {
            sticker: false
        }

	});
}


function showConfirm(_title, _text, _callBack){
   par1=arguments[3];
   bootbox.confirm({
    title: _title,
    message: _text,
    backdrop: true,
    callback: function(result) { if(result) _callBack(par1); }
  });
}


