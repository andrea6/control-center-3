<?php

namespace Recursive\SettingsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class SettingsController extends Controller
{
    /**
     * @Route("/websitesettings/{file}")
     * @Template()
     */
    public function indexAction($file)
    {
        $fn = 'settings/'.$file.'.txt';

        if(isset($_POST) && sizeof($_POST)>0) {
          $handle=  fopen($fn,'w');
          $settings = array();
          foreach($_POST as $key => $value) {
            array_push($settings,["name"=>$key,"value"=>$value]) ;
          }
          fwrite($handle,json_encode($settings));
          fclose($handle);
        }

        // Apre il file e ritorna il contenuto per popolare i campi del form
        $content = json_decode(file_get_contents($fn) );

        $helpcontent=file_get_contents('settings/'.$file.'.html','r');

        // Aggiunge anche il contenuto del file descrizione settings, incluso nella stessa directory
        return array('file'=>$file, 'settings' => $content, 'help' => $helpcontent);
    }
}
