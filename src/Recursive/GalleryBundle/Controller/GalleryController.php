<?php

namespace Recursive\GalleryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GalleryController extends Controller
{
    /**
     * @Route("/gallery/{gallery_name}")
     * @Template()
     */
    public function galleryAction($gallery_name)
    {
      if(!count(glob($this->get('kernel')->getRootDir()."/../web/uploads/".$gallery_name."/*"))==0)
          $files= array_diff(scandir($this->get('kernel')->getRootDir()."/../web/uploads/".$gallery_name,SCANDIR_SORT_ASCENDING),array('..','.','thumbnails'));
      else
          $files=array();
      return array('gallery_name'=>$gallery_name,'files'=>$files);
    }

    /**
     * @Route("/gallery/{gallery_name}/edit")
     * @Template()
     */
    public function editsnipAction($gallery_name)
    {
      return array('gallery_name'=>$gallery_name);
    }

    /**
     * @Route("/gallery/{gallery_name}/edit/filter")
     */
    public function filterAction(Request $request, $gallery_name) {
       $tp=$request->request->get('filename','default');
       $filter=$request->request->get('filter','grayscale');
       $f=IMG_FILTER_GRAYSCALE;
       switch($filter){
         case 'grayscale':
            $f=IMG_FILTER_GRAYSCALE;
            break;
         case 'blur':
            $f=IMG_FILTER_GAUSSIAN_BLUR;
            break;

       }

       $response = new Response();
       try {
            $dir=$this->get('kernel')->getRootDir()."/../web/uploads/".$gallery_name;
            $fname=basename($tp);
            $tp=$dir.'/'.$fname;

            $image_or=imagecreatefromjpeg($tp);
            imagefilter($image_or, $f);
            imagejpeg($image_or,$dir.'/'.$fname,75);
            $response = new Response();
            $response->setContent($tp);
            return $response;
          }
          catch (Exception $e) { // Se proprio tutto va storto
            $response = $event->getResponse();
            $response->setSuccess(false);
            return $response;
          }
    }

    /**
     * @Route("/gallery/{gallery_name}/edit/rotate")
     */
    public function rotateAction(Request $request, $gallery_name) {
       $tp=$request->request->get('filename','default');
       $angle=$request->request->get('angle',0);
       $response = new Response();
       try {
            $dir=$this->get('kernel')->getRootDir()."/../web/uploads/".$gallery_name;
            $fname=basename($tp);
            $tp=$dir.'/'.$fname;

            $image_or=imagecreatefromjpeg($tp);
            list($width, $height) = getimagesize($tp);
            $rap=$width/$height;

            $rotate = imagerotate($image_or, $angle, 0);

            imagejpeg($rotate,$dir.'/'.$fname,75);
            $response = new Response();
            $response->setContent($tp);
            return $response;
          }
          catch (Exception $e) { // Se proprio tutto va storto
            $response = $event->getResponse();
            $response->setSuccess(false);
            return $response;
          }
    }

    /**
     * @Route("/gallery/{gallery_name}/edit/thumbnails")
     */
    public function thumbnailsAction(Request $request, $gallery_name) {
       $tp=$request->request->get('filename','default');
       $response = new Response();
       try {
            $dir=$this->get('kernel')->getRootDir()."/../web/uploads/".$gallery_name;
            $fname=basename($tp);
            $tp=$dir.'/'.$fname;
            if(!file_exists($dir.'/thumbnails'))
                mkdir($dir.'/thumbnails');
            if(!file_exists($dir.'/thumbnails/medium'))
                mkdir($dir.'/thumbnails/medium');
            if(!file_exists($dir.'/thumbnails/small'))
                mkdir($dir.'/thumbnails/small');
            if(!file_exists($dir.'/thumbnails/large'))
                mkdir($dir.'/thumbnails/large');

            $width_medium=300; // yay!
            $width_small=100;
            $width_large=800;
            $image_or=imagecreatefromjpeg($tp);
            list($width, $height) = getimagesize($tp);
            $rap=$width/$height;
            //Small thumbnail
            $image_new=imagecreatetruecolor($width_small,$width_small/$rap);
            imagecopyresampled($image_new,$image_or,0,0,0,0,$width_small,$width_small/$rap,$width,$height);
            imagejpeg($image_new,$dir.'/thumbnails/small/'.$fname,75);
            // Medium thumbnail
            $image_new=imagecreatetruecolor($width_medium,$width_medium/$rap);
            imagecopyresampled($image_new,$image_or,0,0,0,0,$width_medium,$width_medium/$rap,$width,$height);
            imagejpeg($image_new,$dir.'/thumbnails/medium/'.$fname,75);
            // LArge Thumbnail
            $image_new=imagecreatetruecolor($width_large,$width_large/$rap);
            imagecopyresampled($image_new,$image_or,0,0,0,0,$width_large,$width_large/$rap,$width,$height);
            imagejpeg($image_new,$dir.'/thumbnails/large/'.$fname,75);
            $response = new Response();
            $response->setContent($tp);
            return $response;
          }
          catch (Exception $e) { // Se proprio tutto va storto
            $response = $event->getResponse();
            $response->setSuccess(false);
            return $response;
          }
    }

    /**
     * @Route("/gallery/{gallery_name}/delete/")
     */
    public function deleteAction(Request $request, $gallery_name)
    {
        $filename=basename($request->request->get('filename','default'));
        $response = new Response();
        try {
            $pathSmall=$this->get('kernel')->getRootDir()."/../web/uploads/".$gallery_name."/thumbnails/small/".$filename;
            $pathMedium=$this->get('kernel')->getRootDir()."/../web/uploads/".$gallery_name."/thumbnails/medium/".$filename;
            $pathLarge=$this->get('kernel')->getRootDir()."/../web/uploads/".$gallery_name."/thumbnails/large/".$filename;
            $pathOriginal=$this->get('kernel')->getRootDir()."/../web/uploads/".$gallery_name."/".$filename;

            if(file_exists($pathSmall))
                unlink($pathSmall);
            if(file_exists($pathMedium))
                unlink($pathMedium);
            if(file_exists($pathLarge))
                unlink($pathLarge);
            if(file_exists($pathOriginal))
                unlink($pathOriginal);            

            $response->setStatusCode(Response::HTTP_OK);
            $response->setContent($request->request->get('filename','default'));

        }
        catch (Exception $e) {
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
            $response->setContent($e->getMessage());
        }
        return $response;
    }




}
