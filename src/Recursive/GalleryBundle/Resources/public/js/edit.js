loadEditableImages()

$('#b_closeEdit').click(function (){
   $('#galleryView').show(500);
   $('#galleryEdit').hide(500);
});

$('#b_refreshThumbs').click(function() { showConfirm("<i class='fa fa-refresh'></i> Conferma operazione",'Il sistema genera in automatico 4 versioni diverse di ogni immagine caricata, per ottimizzare le prestazioni. In casi molto rari pu&ograve; accadere che tali versioni generate automaticamente contengano degli errori. Se si verificano problemi di visualizzazione delle immagini sul sito, pu&ograve; essere utile effettuare una rigenerazione manuale.<br>L&prime;operazione non ha controindicazioni.<br><br>Rigenerare le seguenti immagini?<br><br>'+getSelectedThumbs('gallery-mini-thumb')+'<br><br>',refreshThumbnails); });
$('#b_rotateLeft').click(function() { showConfirm("<span class='fa fa-rotate-left'></span> Conferma rotazione",'Le seguenti immagini <br><br>'+getSelectedThumbs('gallery-mini-thumb')+'<br><br>verranno ruotate a <b>sinistra</b> di 90 gradi. Confermare?',rotateSelected, 90); });
$('#b_rotateRight').click(function() { showConfirm("<span class='fa fa-rotate-left'></span> Conferma rotazione",'Le seguenti immagini <br><br>'+getSelectedThumbs('gallery-mini-thumb')+'<br><br>verranno ruotate a <b>destra</b> di 90 gradi. Confermare?',rotateSelected, 270); });
$('#b_grayScale').click(function() { showConfirm("<span class='fa fa-circle-o'></span> Conferma filtraggio",'Le seguenti immagini <br><br>'+getSelectedThumbs('gallery-mini-thumb')+'<br><br>verranno <b>convertite in scala di grigi</b>. Confermare?',filterSelected, 'grayscale'); });
$('#b_blur').click(function() { showConfirm("<span class='fa fa-circle-o'></span> Conferma filtraggio",'Le seguenti immagini <br><br>'+getSelectedThumbs('gallery-mini-thumb')+'<br><br>verranno <b>sfuocate</b> utilizzando la tecnica del Gaussian Blur. Confermare?',filterSelected, 'blur'); });


function loadEditableImages() {
  var d= new Date();
  var ret='';
  for(var c=0;c<selected.length;c++){
    ret+='<a class="lblink" href="'+galleryThumbLargePath+selected[c].replace(/^.*[\\\/]/, '')+'?'+d.getTime()+'" data-lightbox="editableImages" data-title="'+galleryPath+selected[c].replace(/^.*[\\\/]/, '')+'" >';
    ret+='<img src="'+ galleryThumbSmallPath+selected[c].replace(/^.*[\\\/]/, '')+'?'+d.getTime() +'" class="gallery-edit-thumb" onerror="this.src=\''+galleryErrorImage+'\'" data-filename="'+galleryPath+selected[c].replace(/^.*[\\\/]/, '')+'" />';
    ret+='</a>'
  }
  $('#edit_thumbs').html(ret)

}


function filterSelected(filter) {
    for(var c=0;c<selected.length;c++){
        (function(fn) { // Faccio la closure con funzione anonima per cambio contesto
          var fname= fn;
          var jqxhr = $.post( galleryFilterPath, { filename: fname, filter: filter } )
                  .done(function(data) {
                        var jqxhr2 = $.post( galleryRefreshThumbnailsPath, { filename: fname } )
                                    .done(function(data2) {
                                       showMessage('Rotazione e rigenerazione thumbs per '+data2.replace(/^.*[\\\/]/, '')+' completato.');
                                       d = new Date();
                                       $(".gallery-image[data-filename='"+galleryPath+data2.replace(/^.*[\\\/]/, '')+"']").attr("src",$(".gallery-image[data-filename='"+galleryPath+data2.replace(/^.*[\\\/]/, '')+"']").attr("src")+'?'+d.getTime());
                                       //$(".gallery-edit-thumb[data-filename='"+galleryPath+data2.replace(/^.*[\\\/]/, '')+"']").attr("src",$(".gallery-edit-thumb[data-filename='"+galleryPath+data2.replace(/^.*[\\\/]/, '')+"']").attr("src")+'?'+d.getTime());
                                       loadEditableImages();
                                    })
                                    .fail(function(error){
                                       showError('Rotazione immagine '+data+' completata. I thumbnails non sono stati aggiornati.');
                                    });
                  })
        })(selected[c]);
      }
}



function rotateSelected(angle) {
    for(var c=0;c<selected.length;c++){
        (function(fn) { // Faccio la closure con funzione anonima per cambio contesto
          var fname= fn;
          var jqxhr = $.post( galleryRotatePath, { filename: fname, angle: angle } )
                  .done(function(data) {
                        var jqxhr2 = $.post( galleryRefreshThumbnailsPath, { filename: fname } )
                                    .done(function(data2) {
                                       showMessage('Rotazione e rigenerazione thumbs per '+data2.replace(/^.*[\\\/]/, '')+' completato.');
                                       d = new Date();
                                       $(".gallery-image[data-filename='"+galleryPath+data2.replace(/^.*[\\\/]/, '')+"']").attr("src",$(".gallery-image[data-filename='"+galleryPath+data2.replace(/^.*[\\\/]/, '')+"']").attr("src")+'?'+d.getTime());
                                       //$(".gallery-edit-thumb[data-filename='"+galleryPath+data2.replace(/^.*[\\\/]/, '')+"']").attr("src",$(".gallery-edit-thumb[data-filename='"+galleryPath+data2.replace(/^.*[\\\/]/, '')+"']").attr("src")+'?'+d.getTime());
                                       loadEditableImages();
                                    })
                                    .fail(function(error){
                                       showError('Rotazione immagine '+data+' completata. I thumbnails non sono stati aggiornati.');
                                    });
                  })
        })(selected[c]);

      }
}


function refreshThumbnails() {
        for(var c=0;c<selected.length;c++){
            var fname= selected[c];
            var jqxhr = $.post( galleryRefreshThumbnailsPath, { filename: fname } )
                    .done(function(data) {
                       showMessage('Aggiornamento '+data.replace(/^.*[\\\/]/, '')+' completato.')
                    });
          }
}