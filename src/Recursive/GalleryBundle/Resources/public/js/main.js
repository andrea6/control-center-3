var selected;
var notice;

  // Document.ready
$(function () {
    selected=[];
    inizializza_uploader();


    $('a').click(function (e){
        if ($(this).attr('href') == '#') {
			e.preventDefault();
		}
     });

    document.body.addEventListener("dragenter", dragin, false);
    document.body.addEventListener("drop", drop, false);
    $('#overlay').click(drop);

    $('.gallery-image').click(selectImage);
    $('#b_Edit').click(showEdit);
    $('#b_SelAll').click(selectAll);
    $('#b_SelNone').click(selectNone);
    $('#b_Delete').click(function() {showConfirm("Conferma cancellazione",'Procedere con la cancellazione delle seguenti immagini?<br><br>'+getSelectedThumbs('gallery-mini-thumb')+'<br><br>L&prime;operazione non pu&ograve; essere annullata.',deletePictures); });

    $(".fancybox").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	400,
		'speedOut'		:	200
	});

    updateUI();

  });


function showEdit() {
  $('#galleryEdit').load(galleryEditViewPath, function() {
    $('#galleryView').hide(500);
    $.getScript(galleryEditScriptPath, function() {
        console.debug('loadedJS')
        $('#galleryEdit').show(500);

    });
  })
}



function deletePictures() {
  for(var c=0;c<selected.length;c++){
    var fname= selected[c];
    var jqxhr = $.post( galleryDeletePath, { filename: fname } )
            .done(function(data) {
               $(".gallery-image[data-filename='"+data+"']").hide(500,function() { $(".gallery-image[data-filename='"+data+"']").remove(); updateUI(); });
               showMessage('Immagine '+data.replace(/^.*[\\\/]/, '')+' cancellata.')
            })
            .fail(function(error){
               updateUI();
            });
  }
}

function getSelectedThumbs(className){
  var ret='';
  for(var c=0;c<selected.length;c++)
    ret+='<img src="'+ galleryThumbSmallPath+selected[c].replace(/^.*[\\\/]/, '')+'?'+(new Date()).getTime() +'" class="'+className+'" onerror="this.src=\''+galleryErrorImage+'\'" data-filename="'+galleryPath+selected[c].replace(/^.*[\\\/]/, '')+'" />';
  return ret;
}

function updateSelection() {
  selected=[];
  $('.gallery-image').each(function(index,element){
     if($(this).hasClass('selected-image'))
       selected.push($(this).data('filename'));
  });
}

function selectAll() {
  $('.gallery-image').each(function(index,element){
     $(this).addClass('selected-image');
  });

  updateUI();
}

function selectNone() {
  $('.gallery-image').removeClass('selected-image');
  updateUI();
}


function selectImage() {
  $(this).toggleClass('selected-image');
  updateUI();
}


function updateUI(){
  updateSelection();
  if(0==selected.length) {
        $('#b_Edit').attr('disabled', 'disabled');
        $('#b_SelNone').attr('disabled', 'disabled');
        $('#b_Delete').attr('disabled', 'disabled');
        $('#labelSelection').fadeOut(200);
  }
  else {
        $('#b_Edit').removeAttr('disabled');
        $('#b_SelNone').removeAttr('disabled');
        $('#b_Delete').removeAttr('disabled');
        $('#labelSelection').html(selected.length+' foto selezionat'+(selected.length == 1 ? 'a' : 'e'));
        $('#labelSelection').fadeIn(200);
  }
}


function dragin(e) {
  $('#overlay').fadeIn(500);
}


function drop(e) {
  $('#overlay').fadeOut(500);
}



function inizializza_uploader() {
  $('#fileupload').fileupload({
          formData: [  ],
          add: function (e, data) {
                      var goUpload = true;
                      var uploadFile=data.files[0]; // La richesta viene generate per ogni file
                      if (!(/\.(jpg|jpeg)$/i).test(uploadFile.name)) {
                          showError('Il file '+uploadFile.name.replace(/^.*[\\\/]/, '')+' non ha un formato valido e varr&agrave; ignorato.')
                          goUpload = false;
                      }
                      if (uploadFile.size > 2000000) { //
                          showError('Il file '+uploadFile.name.replace(/^.*[\\\/]/, '')+' eccede i 2 MBytes di dimensione  e varr&agrave; ignorato.');
                          goUpload = false;
                      }
                      if (goUpload == true) {
                          data.submit();
                      }
                  },

           progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                var options = {
                    text: progress + "% completato."
                };
                options.title=false;
                if (progress > 80)
                  options.title = "Quasi fatto";
                if (progress>=100) {
                  options.title = '<span class="fa fa-check"></span>&nbsp;Foto caricate!';
                  options.type = "success";
                  options.hide = true;
                  options.buttons = {
                      closer: true,
                      sticker: true
                  };
                  options.icon = 'picon picon-task-complete';
                }
                notice.update(options);
            },
           done: function (e, data) {
                var ret=JSON.parse(data.result);
                var jqxhr = $.post( galleryRefreshThumbnailsPath, { filename: ret.filename_realpath } )
                    .done(function(data) {
                       $('#simple_gallery').append('<a href="#" ><img class="gallery-image" src="'+galleryThumbMediumPath+ret.filename+'" data-filename="'+galleryPath+ret.filename+'" ></a>');
                       $(".gallery-image[data-filename='"+galleryPath+ret.filename+"']").click(selectImage);
                       showMessage('Thumbnails create per '+ret.filename.replace(/^.*[\\\/]/, ''))
                       updateUI();
                    })
                    .fail(function(error){
                       updateUI()
                    });

            },
           start: function (e, data) {
             notice = new PNotify({
                          title: "Attendere..",
                          type: 'info',
                          icon: 'picon picon-throbber',
                          hide: false,
                          buttons: {
                              closer: false,
                              sticker: false
                          },
                      });
            }
      });
}