<?php

namespace Recursive\GalleryBundle\EventListener;

use Oneup\UploaderBundle\Event\PostUploadEvent;
use Oneup\UploaderBundle\Event\PreUploadEvent;
use Oneup\UploaderBundle\Event\PostPersistEvent;
use Symfony\Component\HttpFoundation\File\File as sfFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;



class UploadListener
{
    private $doctrine;


    public function __construct($doctrine )
    {
        $this->doctrine = $doctrine;
    }

    /**
    * This function is the uplaod callback. It is called once everytime a file is done uploading.
    *
    */
    public function onUpload(PostPersistEvent $event)
        {
            $response = $event->getResponse();
            $response['filename']=$event->getFile()->getFilename();
            $response['filename_realpath']=$event->getFile()->getRealPath();   
            return $response;

        /*  getFile: Get the uploaded file. Is either an instance of Gaufrette\File or Symfony\Component\HttpFoundation\File\File.
            getRequest: Get the current request including custom variables.
            getResponse: Get the response object to add custom return data.
            getType: Get the name of the mapping of the current upload. Useful if you have multiple mappings and EventListeners.
            getConfig: Get the config of the mapping.
        */

        }

}